package main

import "fmt"
import "reflect"

func main() {
	ls := []int{4, 2, 0, 3, 1}
	ans := []int{0, 1, 2, 3, 4}

	sorted := bubble(ls)

	if reflect.DeepEqual(sorted, ans) {
		fmt.Println("Congtat!")
	} else {
		fmt.Println("expected: ", ans)
		fmt.Println("result: ", sorted)
	}
}

func bubble(ls []int) []int {
	i := 0
	var x int
	var r int
	end := len(ls) - 1

	for i < end {
		r = end
		for r > i {
			if ls[r-1] > ls[r] {
				x = ls[r]
				ls[r] = ls[r-1]
				ls[r-1] = x
			}
			r--
		}
		i++
	}
	return ls
}
