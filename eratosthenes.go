package main

import "fmt"

func main() {

	input := 99999

	var (
		i      int
		k      int
		result []int
	)
	ls := make([]int, input+1, input+1)

	k = 2

	for k*k <= input {
		i = k
		if ls[k] == 0 {
			for i <= input/k {
				ls[k*i] = 1
				i++
			}
		}
		k++
	}
	for i, v := range ls {
		if i == 0 || i == 1 {
			continue
		}
		if v == 0 {
			result = append(result, i)
		}
	}

	fmt.Println(result)
}
