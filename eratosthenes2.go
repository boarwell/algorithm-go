package main

import "fmt"

func main() {
	ls := sieve(100)
	primes := bin2num(ls)

	fmt.Println(primes)

}

func sieve(input int) []int {
	p := 2
	var i int
	ls := make([]int, input+1, input+1)

	for p*p <= input {
		if ls[p] == 0 {
			i = p
			for p <= input/i {
				ls[p*i] = 1
				i++
			}
		}
		p++
	}
	return ls
}

// [0 0 1 0 1 1 1 0 0]
// [2 4 5 6]
func bin2num(ls []int) (result []int) {
	for i, v := range ls {
		if i == 0 || i == 1 {
			continue
		}

		if v == 0 {
			result = append(result, i)
		}
	}

	return result
}
