package main

import "fmt"

func main() {
	i := euclid(234, 423)

	fmt.Println(i)

}

func bigsmall(x, y int) (int, int) {
	if x >= y {
		return x, y
	}
	// 最初はif-elseにしていたが、
	// この場合はelseブロックがいらないよ、
	// とリンターが教えてくれた。
	return y, x
}

func euclid(x, y int) int {
	if x == y {
		return x
	}

	result := 1
	big, small := bigsmall(x, y)

	for result != 0 {
		result = big % small
		if result != 0 {
			big = small
			small = result
		}
	}
	return small
}
