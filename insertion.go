package main

import "fmt"
import "reflect"

func main() {
	ls := []int{4, 2, 0, 3, 1}
	ans := []int{0, 1, 2, 3, 4}

	sorted := insertion2(ls)

	if reflect.DeepEqual(sorted, ans) {
		fmt.Println("Congtat!")
	} else {
		fmt.Println("expected: ", ans)
		fmt.Println("result: ", sorted)
	}
}

func insertion(ls []int) []int {
	t := 1
	i := t
	e := len(ls)
	var x int

	for t < e {
		x = ls[t]
		for i > 0 && ls[i-1] > x {
			ls[i] = ls[i-1]
			i--
		}
		ls[i] = x
		t++
		i = t
	}
	return ls
}

func insertion2(ls []int) []int {
	t := 1
	e := len(ls)
	var (
		i int
		x int
	)

	for t < e {
		i = t
		x = ls[t]
		for i > 0 && ls[i-1] > x {
			ls[i] = ls[i-1]
			i--
		}
		ls[i] = x
		t++
	}
	return ls
}
