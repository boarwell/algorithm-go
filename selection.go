package main

import "fmt"
import "reflect"

func main() {
	ls := []int{4, 2, 0, 3, 1}
	ans := []int{0, 1, 2, 3, 4}

	sorted := insertion2(ls)

	if reflect.DeepEqual(sorted, ans) {
		fmt.Println("Congtat!")
	} else {
		fmt.Println("expected: ", ans)
		fmt.Println("result: ", sorted)
	}
}

func selection(ls []int) []int {
	x := 0
	var (
		a int
		i int
	)

	for x < len(ls) {
		a = min(ls[x:])
		i = indexof(a, ls)
		if i != x {
			swap(x, i, ls)
		}
		x++
	}
	return ls
}

func min(ls []int) int {
	i := 1
	x := ls[0]

	for i < len(ls) {
		if ls[i] < x {
			x = ls[i]
		}
		i++
	}
	return x
}

func tmin() {
	fmt.Print("test of min(): ")
	ls := []int{3, 4, 2, 1}
	min := min(ls)

	if min == 1 {
		fmt.Println("ok")
	} else {
		fmt.Println("boo")
	}
}

func indexof(target int, ls []int) int {
	for i, v := range ls {
		if v == target {
			return i
		}
	}
	return -1
}

func tindexof() {
	ls := []int{3, 4, 2, 1}
	t1 := 3
	t2 := 2

	fmt.Print("test of indexof(): ")
	if t1 == indexof(1, ls) && t2 == indexof(2, ls) {
		fmt.Println("ok")
	} else {
		fmt.Println("boo")
	}
}

func swap(x, y int, ls []int) []int {
	var z int
	z = ls[x]
	ls[x] = ls[y]
	ls[y] = z

	return ls
}
