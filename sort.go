package main

import "fmt"
import "reflect"

func main() {
	ls := []int{4, 5, 6, 1, 4, 3, 8, 5}
	ans := []int{1, 3, 4, 4, 5, 5, 6, 8}

	sorted := insertion(ls)

	if reflect.DeepEqual(ans, sorted) {
		fmt.Println("Congratuation!!")
	} else {
		fmt.Println("I have a bad feeling about this...")
	}

	fmt.Println("the input:  ", ls)
	fmt.Println("the result: ", sorted)

}

// 2017-08-19
func findmin(list []int) (index int) {
	lenls := len(list)
	min := 0
	i := min + 1

	for i < lenls {
		if list[min] > list[i] {
			min = i
		}
		i++
	}
	return min
}

// 2017-08-19
// 選択ソート
func selectionSort(ls []int) []int {
	lenls := len(ls)

	min := 0 // index of temporary min value
	var k int
	var swap int

	for i := 0; i < lenls; i++ {
		k = i + 1
		for k < lenls {
			if ls[min] > ls[k] {
				min = k
			}
			k++
		}
		// 最小値をリストの最初に持ってくる
		swap = ls[i]
		ls[i] = ls[min]
		ls[min] = swap
	}
	return ls
}

// 2017-09-07
// 単純交換ソート（バブルソート）
func bubble(ls []int) []int {
	lastIndex := len(ls) - 1
	right := lastIndex
	tobeFixed := 0
	var swap int

	for tobeFixed < lastIndex {
		for right > tobeFixed {
			if ls[right-1] > ls[right] {
				swap = ls[right-1]
				ls[right-1] = ls[right]
				ls[right] = swap
			}
			right--
		}
		tobeFixed++
		// 最初はこれを書かなかったためソートがされなかった
		right = lastIndex
	}

	return ls
}

// 2017-09-21 復習
func bubble2(ls []int) []int {
	// 数字入れ替えのロジック
	var swap int
	lastIndex := len(ls) - 1
	right := lastIndex

	tobeFixed := 0

	for tobeFixed < lastIndex {
		for right > tobeFixed {
			// 数字入れ替えのロジック
			if ls[right] < ls[right-1] {
				swap = ls[right]
				ls[right] = ls[right-1]
				ls[right-1] = swap
			}
			right--
		}
		tobeFixed++
		right = lastIndex
	}
	return ls
}

// 2017-09-21
// 単純挿入法
func insertion(ls []int) []int {
	target := 1
	var x int
	var k int

	for target < len(ls) {
		x = ls[target]
		k = target

		for k > 0 && ls[k-1] > x {
			ls[k] = ls[k-1]
			k--
		}
		ls[k] = x
		target++
	}

	/*
		for fixed < lastIndex {
			if ls[fixed] > ls[target] {
				for insertion >
				swap = ls[target]
				ls[target] = ls[fixed]
				ls[fixed] = swap
			}
			fixed++
			target++
		}
	*/
	return ls
}

func myinsertion(ls []int) []int {
	var tmp int
	fixed := 0
	compare := 0

	tmp = ls[fixed+1]
	if ls[compare] > ls[fixed+1] {
		ls[compare+1] = ls[compare]
	}
	compare--

	if compare >= 0 {
		if ls[compare] > ls[fixed+1] {
			ls[compare+1] = ls[compare]
		}
	}
	ls[fixed] = tmp
	fixed++

	tmp = ls[fixed+1]
	if ls[compare] > ls[fixed+1] {
		ls[fixed+1] = ls[fixed]
	}

}
